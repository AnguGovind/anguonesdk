//
//  EventEmitter.swift
//  RNAngusdkone
//
//  Created by angusamy-8537 on 12/08/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

import Foundation

@objc(EventEmitter)
class EventEmitter : RCTEventEmitter{
    override init() {
        super.init()
        RNAnguSDK.sharedInstance.registerEventEmitter(eventEmitter: self)
    }
    
    @objc open override func supportedEvents() -> [String] {
        return RNAnguSDK.sharedInstance.allEvents
    }
}

