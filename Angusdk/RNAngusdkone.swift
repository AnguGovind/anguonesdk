//
//  RNAngusdkone.swift
//  RNAngusdkone
//
//  Created by angusamy-8537 on 12/08/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

import Foundation
import Mobilisten

@objc(RNAnguSDK)class RNAnguSDK: NSObject, ActivityHandlerDelegate{
    
    public static var sharedInstance = RNAnguSDK()
    private static var eventEmitter : EventEmitter!
    
    @objc func initIos(_ appkey : String, accesskey: String) {
        DispatchQueue.main.async {
            let chatActivityHandler = ActivityHandler()
            ZohoSalesIQ.Admin.setChatHandler(chatActivityHandler)
            chatActivityHandler.delegate = self
            ZohoSalesIQ.initWithAppKey(appkey, accessKey: accesskey)
            ZohoSalesIQ.showLiveChat(true)
        }
    }
    
    func registerEventEmitter(eventEmitter: EventEmitter) {
        RNAnguSDK.eventEmitter = eventEmitter
    }
    
    func sendEvent(name: String, body: Any?) {
        DispatchQueue.main.async {
            RNAnguSDK.eventEmitter.sendEvent(withName: name, body: body)
        }
    }
    
    var allEvents: [String] = {
        var allEventNames: [String] = ["WidgetAction","VisitorAttended","VisitorMissed","OperatorsOffline","OperatorsOnline","ChatComplete","Rating", "FeedBack","UnreadMessages"]
        return allEventNames
    }()
    
    func handleWidgetAction(isOpen: Bool) {
        print("Chat Event Handler --> handleWidgetAction")
        let map = ["isopen":isOpen]
        sendEvent(name: "WidgetAction", body: map)
    }
    
    func handleVisitorAttended(visitor: SIQVisitorChat) {
        print("Chat Event Handler --> handleVisitorAttended")
        let map = ["VisitID":visitor.visitId, "AttenderEmail":visitor.attenderEmail, "Question":visitor.question]
        sendEvent(name: "VisitorAttended", body: map)
    }
    
    func handleVisitorMissed(visitor: SIQVisitorChat) {
        print("Chat Event Handler --> handleVisitorMissed")
        let map = ["VisitID":visitor.visitId, "Question":visitor.question]
        sendEvent(name: "VisitorMissed", body: map)
    }
    
    func handleAgentsOffline() {
        print("Chat Event Handler --> handleAgentsOffline")
        sendEvent(name: "OperatorsOffline", body: nil)
    }
    
    func handleAgentsOnline() {
        print("Chat Event Handler --> handleAgentsOnline")
        sendEvent(name: "OperatorsOnline", body: nil)
    }
    
    func handleChatComplete(visitor: SIQVisitorChat) {
        print("Chat Event Handler --> handleChatComplete")
        let map = ["VisitID":visitor.visitId, "AttenderEmail":visitor.attenderEmail, "Question":visitor.question]
        sendEvent(name: "ChatComplete", body: map)
    }
    
    func handleRating(visitor: SIQVisitorChat) {
        print("Chat Event Handler --> handleRating")
        let map = ["VisitID":visitor.visitId, "AttenderEmail":visitor.attenderEmail, "Question":visitor.question, "Rating":visitor.rating]
        sendEvent(name: "Rating", body: map)
    }
    
    func handleFeedback(visitor: SIQVisitorChat) {
        print("Chat Event Handler --> handleFeedback")
        let map = ["VisitID":visitor.visitId, "AttenderEmail":visitor.attenderEmail, "Question":visitor.question, "FeedBack":visitor.feedback]
        sendEvent(name: "FeedBack", body: map)
    }
    
    func updateUnreadMessages(count: Int) {
        print("Chat Event Handler --> updateUnreadMessages")
        sendEvent(name: "UnreadMessages", body: count)
    }
    
    @objc func setTitle(_ title : String){
        DispatchQueue.main.async {
            ZohoSalesIQ.Chat.setTitle(title);
        }
    }
    
    @objc func setprimarycolor(_ r: Int, g: Int, b : Int){
        ZohoSalesIQ.Chat.setThemeColor(UIColor(red: CGFloat(r), green: CGFloat(g), blue: CGFloat(b), alpha: 1));
    }
    
    @objc func setlanguage(_ code: String){
        ZohoSalesIQ.Chat.setLanguage(code);
    }
    
    @objc func setDepartment(_ department : String){
        ZohoSalesIQ.Chat.setDepartment(department);
    }
    
    @objc func setOperatorEmail(_ email : String){
        ZohoSalesIQ.Chat.setAgentEmail(email)
    }
    
    @objc func canshowOperatorImageinLauncher(_ show : Bool){
        ZohoSalesIQ.Chat.setVisibility(.agentPhotoOnChatIcon, visible: show)
    }
    
    @objc func showChat(){
        ZohoSalesIQ.Chat.show()
    }
    
    @objc func showFloat(_ show: Bool){
        ZohoSalesIQ.showLiveChat(show)
    }
    
    @objc func setVisitorDetails(_ name : String, email : String, number: String){
        ZohoSalesIQ.Visitor.setName(name);
        ZohoSalesIQ.Visitor.setEmail(email);
        ZohoSalesIQ.Visitor.setContactNumber(number);
    }
    
    @objc func visitoraddinfo(_ key : String, value : String){
        ZohoSalesIQ.Visitor.addInfo(key, value: value)
    }
    
    @objc func setQuestion(_ question: String){
        ZohoSalesIQ.Visitor.setQuestion(question)
    }
    
    @objc func startChat(_ message : String){
        ZohoSalesIQ.Chat.startChat(question: message)
    }
    
    @objc func setPageTitle(_ pagetitle : String){
        ZohoSalesIQ.Tracking.setPageTitle(pagetitle)
    }
    
    @objc func setCustomAction(_ action_string : String){
        ZohoSalesIQ.Tracking.setCustomAction(action_string)
    }
    
    //  @objc func setConversationPageTitle(_ conv_title : String){
    //
    //  }
    
}

class ActivityHandler: ChatActivityHandler
{
    weak var delegate : ActivityHandlerDelegate?
    override func handleWidgetAction(isOpen: Bool)
    {
        delegate?.handleWidgetAction(isOpen: isOpen)
    }
    override func handleVisitorAttended(visitor: SIQVisitorChat) {
        delegate?.handleVisitorAttended(visitor: visitor)
    }
    override func handleVisitorMissed(visitor: SIQVisitorChat) {
        delegate?.handleVisitorMissed(visitor: visitor)
    }
    override func handleAgentsOffline() {
        delegate?.handleAgentsOffline()
    }
    override func handleAgentsOnline() {
        delegate?.handleAgentsOnline()
    }
    override func handleChatComplete(visitor: SIQVisitorChat) {
        delegate?.handleChatComplete(visitor: visitor)
    }
    override func handleRating(visitor: SIQVisitorChat) {
        delegate?.handleRating(visitor: visitor)
    }
    override func handleFeedback(visitor: SIQVisitorChat) {
        delegate?.handleFeedback(visitor: visitor)
    }
    override func updateUnreadMessages(count: Int) {
        delegate?.updateUnreadMessages(count: count)
    }
}

protocol ActivityHandlerDelegate : class{
    func handleWidgetAction(isOpen: Bool)
    func handleVisitorAttended(visitor : SIQVisitorChat)
    func handleVisitorMissed(visitor: SIQVisitorChat)
    func handleAgentsOffline()
    func handleAgentsOnline()
    func handleChatComplete(visitor: SIQVisitorChat)
    func handleRating(visitor: SIQVisitorChat)
    func handleFeedback(visitor: SIQVisitorChat)
    func updateUnreadMessages(count: Int)
}
