#import "React/RCTBridgeModule.h"
#import <Mobilisten/Mobilisten.h>
#import <React/RCTEventEmitter.h>

@interface RCT_EXTERN_MODULE(RNAnguSDK, NSObject)
+ (BOOL)requiresMainQueueSetup
{
    return YES;
}
RCT_EXTERN_METHOD(initIos: (NSString *)appkey accesskey:(NSString *)accesskey)
RCT_EXTERN_METHOD(setTitle : (NSString)title)
RCT_EXTERN_METHOD(setprimarycolor: (NSInteger *)r g:(NSInteger *)g b:(NSInteger *)b)
RCT_EXTERN_METHOD(setlanguage: (NSString)color)
RCT_EXTERN_METHOD(setDepartment: (NSString)department)
RCT_EXTERN_METHOD(setOperatorEmail: (NSString)email)
RCT_EXTERN_METHOD(canshowOperatorImageinLauncher: (NSNumber)show)
RCT_EXTERN_METHOD(showChat)
RCT_EXTERN_METHOD(showFloat: (NSNumber)show)
RCT_EXTERN_METHOD(setVisitorDetails: (NSString *)name email:(NSString *)email number:(NSString *)number)
RCT_EXTERN_METHOD(visitoraddinfo: (NSString *)key value:(NSString *)value)
RCT_EXTERN_METHOD(setQuestion: (NSString)question)
RCT_EXTERN_METHOD(startChat: (NSString)message)
RCT_EXTERN_METHOD(setPageTitle: (NSString)pagetitle)
RCT_EXTERN_METHOD(setCustomAction: (NSString)action_string)
RCT_EXTERN_METHOD(setConversationPageTitle: (NSString)conv_page_title)
@end

