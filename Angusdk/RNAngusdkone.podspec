
Pod::Spec.new do |s|
  s.name         = "RNAngusdkone"
  s.version      = "1.0.0"
  s.summary      = "RNAngusdkone"
  s.description  = <<-DESC
                  Angu sdk is very useful one
                   DESC
  s.homepage     = "https://zoho.com/salesiq"
  s.license      = { :type => 'MIT', :text=> <<-LICENSE
    MIT License
    Copyright (c) 2019 Angu <angusamy.g@zohocorp.com>
    
    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:
      
      The above copyright notice and this permission notice shall be included in
      all copies or substantial portions of the Software.
      
      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
      THE SOFTWARE.
      LICENSE
      }
  s.author             = { "angu" => "angusamy.g@zohocorp.com" }
  s.platform     = :ios, "8.0"
  s.source       = { :git => 'file:///Users⁩/angusamy-8537⁩/⁨Desktop⁩/⁨reactnativelibrary⁩/angusdktwo' }
  s.source_files  = "ios/*.{h,m}"
  s.requires_arc = true
  s.dependency "React"
  #s.dependency "others"

end

  
