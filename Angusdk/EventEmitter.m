//
//  EventEmitter.m
//  RNAngusdkone
//
//  Created by angusamy-8537 on 12/08/19.
//  Copyright © 2019 Facebook. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>

@interface RCT_EXTERN_MODULE(EventEmitter, RCTEventEmitter)
+ (BOOL)requiresMainQueueSetup
{
    return YES;
}
RCT_EXTERN_METHOD(supportedEvents)

@end
